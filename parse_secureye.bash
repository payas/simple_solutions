awk '{ print $4,$5,$11,$12 }' $1 | sed '/1 1 Time/d' | sed -e '1,5d' | awk 'function prtBuf(        idx) {
    if (cnt > 0) {
        idx = int((rand() * cnt) + 1)
        print buf[idx]
    }
    cnt = 0
}

BEGIN { srand() }
#$1 == prev && substr($4, 0, 2) != substr(pl, 0, 2) { prtBuf() }
#$1 != prev { prtBuf() }
{ if ($1 != prev || $2 != prev2)
        { prtBuf() }
  else if ($1 == prev && $2 == prev2 && $3 != prev3)
        { prtBuf() }
  else if ($1 == prev && $2 == prev2 && $3 == prev3 && substr($4, 0, 2) != substr(pl, 0, 2))
        { prtBuf() }
}
        { buf[++cnt]=$0; prev=$1; prev2=$2; prev3=$3; pl=$4 }
END { prtBuf() }
'
